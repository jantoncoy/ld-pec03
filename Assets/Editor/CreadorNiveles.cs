using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CreadorNiveles
{
    public class tileBox
    {
        /// <summary>
        /// Ancho de la imagen 1:1
        /// </summary>
        public int ancho = 20;

        /// <summary>
        /// Textura que representa este prefab
        /// </summary>
        private Texture2D image;

        /// <summary>
        /// Estados de la imagen
        /// </summary>
        private GUIStyle estilos;

        /// <summary>
        /// Posicion en x
        /// </summary>
        public int x;

        /// <summary>
        /// Posicion en y
        /// </summary>
        public int y;

        /// <summary>
        /// GameObject de este elemento
        /// </summary>
        public GameObject prefab;

        private Texture2D recuperarImagen(Sprite sprite)
        {
            Rect rect = sprite.rect;
            Texture2D texture = sprite.texture;
            Color[] pixeles = texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
            Texture2D imagen = new Texture2D((int)rect.width, (int)rect.height);
            imagen.SetPixels(pixeles);
            imagen.Apply();
            return imagen;
        }

        public tileBox()
        {
            cogerImagen();
        }

        private void cogerImagen()
        {
            if (prefab == null)
                image = recuperarImagen(((GameObject)Resources.Load("Prefabs/Empty")).GetComponent<SpriteRenderer>().sprite);
            else
                image = recuperarImagen(prefab.GetComponent<SpriteRenderer>().sprite);

            estilos = new GUIStyle();
            estilos.normal.background = image;
        }

        public bool pintarse()
        {
            return GUI.Button(new Rect(x*ancho, y*ancho,ancho,ancho),"",estilos);
        }

        public void cambiarPrefab(GameObject nuevoPrefab)
        {

            if(nuevoPrefab != null)
            {
                Debug.Log(nuevoPrefab.name);
            }
            else
            {
                Debug.Log("prefab nulo");
            }

            prefab = nuevoPrefab;
            cogerImagen();
            pintarse();
        }
    }

    public class CreadorNivelesEditor : EditorWindow
    {
        /// <summary>
        /// Titulo de la ventana
        /// </summary>
        public static string tituloWindows = "Creador de niveles";
        /// <summary>
        /// Nombre del nivel
        /// </summary>
        private string tituloNivel = "Nivel_0";
        /// <summary>
        /// Ruta donde se guarda y se recupera el nivel
        /// </summary>
        private string rutaGuardadoNivel = "Assets/Resources/Niveles/";
        /// <summary>
        /// Longitud del nivel
        /// </summary>
        private int longitud = 30;
        /// <summary>
        /// Ancho del tile en el modo edicion
        /// </summary>
        private int ancho = 34;
        /// <summary>
        /// Posiciones del jugador
        /// </summary>
        private int jugadorX = 0, jugadorY = 0;
        /// <summary>
        /// Musica que llevara el nivel
        /// </summary>
        private AudioClip backgroundNivel = null;
        /// <summary>
        /// Lista de gameobjects que formaran los prefab para el nivel
        /// </summary>
        private List<GameObject> listaGameObjects = new List<GameObject>();
        /// <summary>
        /// Indica si esta en modo edicion o configuracion
        /// </summary>
        private bool edicion = false;
        /// <summary>
        /// Posicion del scroll de la ventana
        /// </summary>
        private Vector2 scroll = Vector2.zero;
        /// <summary>
        /// Array de tiles 
        /// </summary>
        private tileBox [,] tiles;
        /// <summary>
        /// Opcion que tenemos seleccionada en las herramientas
        /// </summary>
        private int opcionHerramienta = 0;
        /// <summary>
        /// Indica si se muestra los eventos
        /// </summary>
        private string mostrarRecuperar = null, mostrarGuardar = null;

        /// <summary>
        /// Muestra la ventana y indica el titulo
        /// </summary>
        public void showWindow()   
        {
            GetWindow<CreadorNivelesEditor>(CreadorNivelesEditor.tituloWindows);
            this.titleContent = new GUIContent(CreadorNivelesEditor.tituloWindows);
        }

        /// <summary>
        /// Contendra los elementos del editor
        /// </summary>
        private void OnGUI()
        {
            
            pintarBotonEdicion();

            if (!edicion)
            {
                scroll = EditorGUILayout.BeginScrollView(scroll, false, false);
                pintarDatosGenerales();
                pintarCrearPrefab();
                pintarBotonGuardado();
                recuperarNivel();
            }
            else
            {
                crearHerramientas();
                EditorGUILayout.BeginHorizontal();
                GUILayoutOption [] guiOption = new GUILayoutOption[2];
                guiOption[1] = GUILayout.Height(longitud*ancho);
                guiOption[0] = GUILayout.Width(longitud*ancho);

                scroll = EditorGUILayout.BeginScrollView(scroll,true,true,guiOption);
                pintarGrid();
                pintarBotones();
                EditorGUILayout.EndHorizontal();
                
            }

            if (GUI.changed)
            {
                Repaint();
            }

            GUILayout.EndScrollView();

        }

        /// <summary>
        /// Dibuja los datos generales en el modo configuracion
        /// </summary>
        private void pintarDatosGenerales()
        {
            GUILayout.Label("Editor de niveles", EditorStyles.boldLabel);
            GUILayout.Label("Configuracion general del nivel", EditorStyles.label);
            tituloNivel = EditorGUILayout.TextField("Nombre del nivel: ", tituloNivel);
            rutaGuardadoNivel = EditorGUILayout.TextField("Ruta de guardado/recuperacion del nivel (Ruta relativa al proyecto): ", rutaGuardadoNivel);
            longitud = EditorGUILayout.IntField("Longitud del nivel: ", longitud);
            backgroundNivel = (AudioClip)EditorGUILayout.ObjectField("Cancion nivel: ", backgroundNivel, typeof(AudioClip), true);
            jugadorX = EditorGUILayout.IntField("Fila de comienzo del jugador: ", jugadorX);
            jugadorY = EditorGUILayout.IntField("Columna de comienzo del jugador: ", jugadorY);
        }

        /// <summary>
        /// Pinta el boton de crear prefabs y su ayuda
        /// </summary>
        private void pintarCrearPrefab()
        {
            if (GUILayout.Button("Agregar prefab"))
            {
                GameObject nuevoGameObject = null;
                //listaRutas.Add(nuevaRuta);
                listaGameObjects.Add(nuevoGameObject);
            }

            GUILayout.Label("Los prefabs se deben encontrar en la carpeta Assets / Resources", EditorStyles.helpBox);
            GUILayout.Label("Seleccione el prefab en el listado que desea agregar y presione en el boton izquierdo del raton para ponerlo en el grid", EditorStyles.helpBox);

            for (int a = 0; a < listaGameObjects.Count; a++)
            {
                Object campo = EditorGUILayout.ObjectField(a + ". Prefab: ", listaGameObjects[a], typeof(GameObject), true);
                listaGameObjects[a] = (GameObject) campo;
            }
        }

        /// <summary>
        /// Pinta el boton de guardado y contiene su logica
        /// </summary>
        private void pintarBotonGuardado()
        {
            if (GUILayout.Button("Guardar nivel"))
            {
                Nivel nivel = new Nivel();
                nivel.nombreNivel = this.tituloNivel;
                nivel.longitud = this.longitud;
                string rutaSonido = AssetDatabase.GetAssetPath(this.backgroundNivel);
                rutaSonido = rutaSonido != null ? rutaSonido.Replace(".prefab", "").Replace("Assets/Resources/", "").Replace(".ogg","").Replace(".mp3","") : "";
                nivel.prefabSonido = rutaSonido;
                nivel.xInicio = jugadorX;
                nivel.yInicio = jugadorY;

                for(int a=0; a < tiles.GetLength(0); a++)
                {
                    for (int b = 0; b < tiles.GetLength(1); b++)
                    {
                        Tile tile = new Tile();
                        tile.x = tiles[a, b].x;
                        tile.y = tiles[a, b].y;
                        string ruta = AssetDatabase.GetAssetPath(tiles[a, b].prefab);
                        ruta = ruta != null ? ruta.Replace(".prefab", "").Replace("Assets/Resources/", "") : "";
                        tile.ruta = ruta;
                        nivel.agregarTile(tile);
                    }
                }

                string nivelString = JsonUtility.ToJson(nivel);
                string path;

                if (rutaGuardadoNivel == null || rutaGuardadoNivel.Trim().Equals(""))
                    path = this.tituloNivel + ".json";
                else
                    path = rutaGuardadoNivel + this.tituloNivel + ".json";

                System.IO.File.WriteAllText(path, nivelString);
                mostrarGuardar = "Guardado en: " +path;
            }

            GUILayout.Label("Con esta opcion puede guardar el nivel.", EditorStyles.helpBox);

            if (mostrarGuardar != null)
            {
                GUILayout.Label(mostrarGuardar, EditorStyles.foldoutHeader);
            }
        }

        /// <summary>
        /// Recupera el nivel
        /// </summary>
        private void recuperarNivel()
        {
            if(GUILayout.Button("Recuperar nivel"))
            {
                string json = null;
                string path;

                if (rutaGuardadoNivel == null || rutaGuardadoNivel.Trim().Equals(""))
                    path = this.tituloNivel + ".json";
                else
                    path = rutaGuardadoNivel + this.tituloNivel + ".json";
                
                json = System.IO.File.ReadAllText(path);

                Nivel nivel = JsonUtility.FromJson<Nivel>(json);
                this.jugadorX = nivel.xInicio;
                this.jugadorY = nivel.yInicio;
                this.longitud = nivel.longitud;
                this.tituloNivel = nivel.nombreNivel;
                AudioClip prefabSonido = (AudioClip)Resources.Load(nivel.prefabSonido);
                this.backgroundNivel = prefabSonido;

                tiles = new tileBox[longitud, longitud];
                inicializarTiles();

                foreach(Tile tile in nivel.tiles)
                {
                    Debug.Log("" + tile.ruta);
                    GameObject prefab = (GameObject)Resources.Load(tile.ruta);
                    if (prefab != null)
                        agregarPrefab(prefab);
                    this.tiles[tile.x,tile.y].cambiarPrefab(prefab);
                }

                mostrarRecuperar = "Recuperado el nivel: " + path;
                
            }

            GUILayout.Label("Con esta opcion puede recuperar el nivel con el nombre de la configuracion, tambien agregara automaticamente sus prefabs al listado.", EditorStyles.helpBox);

            if(mostrarRecuperar != null)
            {
                GUILayout.Label(mostrarRecuperar,EditorStyles.foldoutHeader);
            }
        }

        /// <summary>
        /// Pinta el boton de edicion y su ayuda
        /// </summary>
        private void pintarBotonEdicion()
        {
            if (GUILayout.Button(edicion ? "Cambiar a datos generales":"Cambiar a edicion"))
            {
                edicion = !edicion;
                scroll = Vector2.zero;
            }
            GUILayout.Label("Puedes alternar entre modo edicion o configuracion", EditorStyles.helpBox);
        }

        /// <summary>
        /// Pinta el grid en el modo edicion
        /// </summary>
        private void pintarGrid()
        {

            Handles.BeginGUI();

            //definimos la dimension por primera vez
            if(tiles == null)
            {
                tiles = new tileBox[longitud,longitud];
                inicializarTiles();

            }
            else if(tiles.GetLength(0) != longitud) //si cambia el ancho cambiamos el tile
            {
                tiles = new tileBox[longitud, longitud];
                inicializarTiles();
            }

            //Pintamos filas
            Handles.color = Color.red;

            for (int a = 0; a <= this.longitud;a++)
            {
                Vector3 inicio = new Vector3(0, (a) * ancho);
                Vector3 final = new Vector3(this.longitud * ancho, (a) * ancho);

                Handles.DrawLine(inicio, final);
            }

            //Pintamos columnas
            Handles.color = Color.green;

            for (int a = 0; a <= this.longitud;a++)
            {
                Vector3 inicio = new Vector3((a) * ancho, 0);
                Vector3 final = new Vector3((a) * ancho, this.longitud * ancho);

                Handles.DrawLine(inicio, final);
            }


            Handles.EndGUI();
        }

        /// <summary>
        /// Crea las herramientas en el modo edicion
        /// </summary>
        private void crearHerramientas()
        {

            Texture2D[] materiales = new Texture2D[listaGameObjects.Count + 1];

            Texture2D image = ((GameObject)Resources.Load("Prefabs/Empty")).GetComponent<SpriteRenderer>().sprite.texture;

            materiales[0] = image;

            for (int a = 0; a < listaGameObjects.Count; a++)
            {
                if (listaGameObjects[a] != null)
                {
                    Sprite sprite = listaGameObjects[a].GetComponent<SpriteRenderer>().sprite;
                    Rect rect = sprite.rect;
                    Texture2D texture = sprite.texture;
                    Color[] pixeles = texture.GetPixels((int)rect.x,(int)rect.y,(int)rect.width,(int)rect.height);
                    Texture2D imagen = new Texture2D((int)rect.width,(int)rect.height);
                    imagen.SetPixels(pixeles);
                    imagen.Apply();
                    materiales[a + 1] = imagen;
                }
            }

            opcionHerramienta = GUILayout.Toolbar(opcionHerramienta, materiales);

            switch (opcionHerramienta)
            {
                default:
                    Debug.Log(" "+opcionHerramienta);
                break;
            }
        }

        /// <summary>
        /// Pinta los tiles en el modo edicion
        /// </summary>
        private void pintarBotones()
        {
            for (int a = 0; a < tiles.GetLength(0); a++)
            {
                for (int b = 0; b < tiles.GetLength(1); b++)
                {
                    if (tiles[a,b] != null)
                        if (tiles[a, b].pintarse())
                        {
                            pulsarTile(tiles[a, b]);
                        }
                }
            }
        }

        /// <summary>
        /// Agrega un prefab al listado si no lo encuentra ya
        /// </summary>
        /// <param name="nuevoPrefab"></param>
        private void agregarPrefab(GameObject nuevoPrefab)
        {
            if(listaGameObjects == null)
            {
                listaGameObjects = new List<GameObject>();
            }

            bool encontrado = false;

            foreach(GameObject prefab in listaGameObjects)
            {
                if(prefab == nuevoPrefab)
                {
                    encontrado = true;
                    break;
                }
            }

            if (!encontrado)
            {
                listaGameObjects.Add(nuevoPrefab);
            }
        }

        /// <summary>
        /// Inicializa los tiles
        /// </summary>
        private void inicializarTiles()
        {
            for (int a = 0; a < tiles.GetLength(0); a++)
            {
                for (int b = 0; b < tiles.GetLength(1); b++)
                {
                    tiles[a,b] = new tileBox();
                    tiles[a, b].ancho = ancho;
                    tiles[a, b].x = a;
                    tiles[a, b].y = b;
                }
            }
        }

        /// <summary>
        /// Rellena un tile con su objeto
        /// </summary>
        /// <param name="tile"></param>
        private void pulsarTile(tileBox tile)
        {
            if (opcionHerramienta == 0)
                tile.cambiarPrefab(null);
            else
                tile.cambiarPrefab(listaGameObjects[opcionHerramienta-1]);

            Debug.Log("Se pulso " + opcionHerramienta);
        }
    }

    /// <summary>
    /// Inicia la opcion del creador de niveles en el menu de Window
    /// </summary>
    public class CreadorNivel
    {
        public static CreadorNivelesEditor editor = null;

        [MenuItem("Window / Creador de niveles")]
        public static void AbrirCreador()
        {
            editor = new CreadorNivelesEditor();
            editor.showWindow();
        }
    }
}

