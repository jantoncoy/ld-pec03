using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tile
{
    /// <summary>
    /// Posicion del tile
    /// </summary>
    public int x, y;

    /// <summary>
    /// ruta del prefab
    /// </summary>
    public string ruta;
}
