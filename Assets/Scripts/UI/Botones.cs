using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Botones : MonoBehaviour
{
    /// <summary>
    /// Carga el juego
    /// </summary>
    public void juego()
    {
        Partida.nivelInt = 0;
        Partida.nivel = Partida.levelsPath+Partida.nivelPath+Partida.nivelInt;
        SceneManager.LoadScene("Nivel");
    }

    /// <summary>
    /// Vuelve al menu de inicio
    /// </summary>
    public void menu()
    {
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// accion cuando se pulsa en salir
    /// </summary>
    public void salir()
    {
        Application.Quit(1);
    }

    /// <summary>
    /// Reinicia el nivel
    /// </summary>
    public void Reiniciar()
    {
        SceneManager.LoadScene("Nivel");
    }
}
