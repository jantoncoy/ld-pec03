using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Nivel
{
    /// <summary>
    /// nombre del nivel
    /// </summary>
    public string nombreNivel;

    /// <summary>
    /// Longitud del nivel
    /// </summary>
    public int longitud;

    /// <summary>
    /// Prefab de sonidos
    /// </summary>
    public string prefabSonido;

    /// <summary>
    /// Posicion del jugador
    /// </summary>
    public int xInicio, yInicio;

    /// <summary>
    /// Tiles del nivel
    /// </summary>
    public List<Tile> tiles;

    /// <summary>
    /// Agrega un tile a la lista de tiles
    /// </summary>
    /// <param name="tile"></param>
    public void agregarTile(Tile tile)
    {
        if(tiles != null)
        {
            tiles.Add(tile);
        }
        else
        {
            tiles = new List<Tile>();
            tiles.Add(tile);
        }
    }
}
