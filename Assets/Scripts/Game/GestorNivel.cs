using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Clase principal que se encarga de gestionar el nivel
/// </summary>
public class GestorNivel : MonoBehaviour
{
    /// <summary>
    /// Registramos un objetivo
    /// </summary>
    public static Action<Objetivo> registrarObjetivo;

    /// <summary>
    /// Suma un paso al contador
    /// </summary>
    public static Action sumarPaso;

    /// <summary>
    /// Suma un empuje el contador
    /// </summary>
    public static Action sumarEmpuje;

    /// <summary>
    /// Lista de objetivos
    /// </summary>
    private List<Objetivo> objetivos;

    /// <summary>
    /// Camara de la escena
    /// </summary>
    public Camera mainCamera;

    /// <summary>
    /// Nivel cargado
    /// </summary>
    private Nivel nivel;

    /// <summary>
    /// Jugador
    /// </summary>
    public GameObject jugador;

    /// <summary>
    /// Cargando de nivel
    /// </summary>
    private bool cargandoNivel = false;

    /// <summary>
    /// Marcadores del juego
    /// </summary>
    public TextMeshProUGUI nivelActual, pasos, empujes, tiempo;

    /// <summary>
    /// Panel de victoria
    /// </summary>
    public GameObject victoria;

    /// <summary>
    /// Indica si se puede sumar un paso o empuje
    /// </summary>
    private bool sePuedePaso = true, sePuedeEmpuje = true;

    /// <summary>
    /// Contadores de tiempo, pasos y empujes
    /// </summary>
    private int segundos, minutos, horas, empujesInt, pasosInt;

    /// <summary>
    /// Formato para textos con tres digitos enteros
    /// </summary>
    private const String formato3D = "{0:000}";

    /// <summary>
    /// Formato para textos con dos digitos enteros
    /// </summary>
    private const String formato2D = "{0:00}";

    /// <summary>
    /// Inicializamos los recursos necesarios
    /// </summary>
    private void Start()
    {
        //inicializamos marcadores de tiempo
        segundos = 0;
        minutos = 0;
        horas = 0;
        victoria.SetActive(false);

        //Inicializamos eventos
        registrarObjetivo = agregarObjetivo;
        sumarEmpuje = masEmpuje;
        sumarPaso = masPasos;

        //Cargamos el nivel
        cargarNivel();

        //Llamamos al contador cada segundo
        InvokeRepeating("contarTiempo", 1, 1);

        //actualizamos el nivel
        nivelActual.text = String.Format(formato2D, Partida.nivelInt);
    }

    /// <summary>
    /// Sumamos un empuje y actualizamos marcador
    /// </summary>
    private void masEmpuje()
    {
        if (sePuedeEmpuje)
        {
            sePuedeEmpuje = false;
            empujesInt += 1;
            empujes.text = String.Format(formato3D, empujesInt);
        }
    }

    /// <summary>
    /// Sumamos un empuje y actualizamos marcador
    /// </summary>
    private void masPasos()
    {
        if (sePuedePaso)
        {
            sePuedePaso = false;
            pasosInt += 1;
            pasos.text = String.Format(formato3D, pasosInt);
        }
    }

    /// <summary>
    /// Utilizamos esta funcion para contar el tiempo
    /// </summary>
    private void contarTiempo()
    {
        //seteamos la suma de empuje y paso
        sePuedeEmpuje = true;
        sePuedePaso = true;

        //sumamos minutos
        if(segundos+1 == 60)
        {
            segundos = 0;
            minutos += 1;
        }
        else
        {
            //sumamos segundos
            segundos += 1;
        }

        //Sumamos horas
        if(minutos >= 60)
        {
            minutos = 0;
            horas += 1;
        }

        tiempo.text = String.Format(formato2D,horas)+":"+string.Format(formato2D, minutos) + ":"+String.Format(formato2D, segundos);
    }

    /// <summary>
    /// Procesamos la camara y si debemos pasar de nivel
    /// </summary>
    private void FixedUpdate()
    {
        //Si los objetivos estan cumplidos se pasa de nivel
        if (comprobarObjetivos() && !cargandoNivel)
        {
            cargandoNivel = true;
            //Pasamos de nivel
            pasarNivel();
        }

        actualizarCamara();
    }

    /// <summary>
    /// Comprueba los objetivos y si estan cumplidos devuelve true
    /// </summary>
    private bool comprobarObjetivos()
    {
        bool resultado = true;

        if(objetivos != null)
        {
            foreach (Objetivo objetivo in objetivos)
            {
                if (!objetivo.cumplido)
                {
                    resultado = false;
                    break;
                }
            }
        }
        else
        {
            resultado = false;
        }

        return resultado;
    }

    /// <summary>
    /// agrega un objetivo a la lista de objetivos
    /// </summary>
    /// <param name="objetivo"></param>
    private void agregarObjetivo(Objetivo objetivo)
    {
        if(this.objetivos == null)
        {
            this.objetivos = new List<Objetivo>();
        }

        objetivos.Add(objetivo);
    }

    /// <summary>
    /// Pasamos de nivel
    /// </summary>
    private void pasarNivel()
    {
        if(Partida.nivelInt+1 == Partida.nivelMax)
        {
            jugador.SetActive(false);
            victoria.SetActive(true);
        }
        else
        {
            Partida.nivelInt += 1;
            Partida.nivel = Partida.levelsPath + Partida.nivelPath + Partida.nivelInt;
            SceneManager.LoadScene("Nivel");
        }
    }

    /// <summary>
    /// Cargamos el nivel
    /// </summary>
    private void cargarNivel()
    {
        TextAsset archivo = Resources.Load<TextAsset>(Partida.nivel);
        string json = archivo.text;
        Nivel nivel = JsonUtility.FromJson<Nivel>(json);
        this.nivel = nivel;

        cargarSonido();
        cargarEscenario();
        cargarJugador();
    }

    /// <summary>
    /// Carga el sonido del nivel
    /// </summary>
    private void cargarSonido()
    {
        AudioClip audioClip = Resources.Load<AudioClip>(nivel.prefabSonido);

        if(audioClip != null)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = audioClip;
            audioSource.loop = true;
            audioSource.volume = 0.30f;
            audioSource.Play();
        }
    }

    /// <summary>
    /// Carga el escenario del nivel
    /// </summary>
    private void cargarEscenario()
    {
        //Nos recorremos los tiles para cargar el nivel
        foreach (Tile tile in nivel.tiles)
        {
            if(tile.ruta != null && !tile.ruta.Equals(""))
            {
                GameObject elemento = (GameObject) Instantiate(Resources.Load(tile.ruta));
                elemento.transform.position = new Vector3(tile.x, tile.y*-1, elemento.transform.position.z);
            }
        }
    }

    /// <summary>
    /// Carga al jugador en el nivel
    /// </summary>
    private void cargarJugador()
    {
        if(nivel != null)
        {
            GameObject elemento = (GameObject)Instantiate(Resources.Load("Prefabs/Personaje"));
            elemento.transform.position = new Vector3(nivel.xInicio, nivel.yInicio*-1, 0f);
            jugador = elemento;
        }
    }

    /// <summary>
    /// Actualiza la camara a la posicion del jugador
    /// </summary>
    private void actualizarCamara()
    {
        if(jugador != null)
        mainCamera.transform.position = new Vector3(jugador.transform.position.x,jugador.transform.position.y,-10);
    }
}
