using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caja : MonoBehaviour
{
    public Rigidbody2D rigid;
    public Color originColor, pressColor; 

    // Start is called before the first frame update
    void Start()
    {
        rigid = this.transform.GetComponent<Rigidbody2D>();
        Color oldColor = this.GetComponent<SpriteRenderer>().color;
        originColor = new Color(oldColor.r, oldColor.g, oldColor.b, oldColor.a);
        pressColor = new Color(1f, 0f, 0f, oldColor.a);
    }

    /// <summary>
    /// Bloquea la caja segun donde se este moviendo el jugador
    /// </summary>
    /// <param name="arriba"></param>
    /// <param name="abajo"></param>
    /// <param name="izquierda"></param>
    /// <param name="derecha"></param>
    public void moverse(bool arriba, bool abajo, bool izquierda, bool derecha)
    {
        if (arriba || abajo)
        {
            rigid.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
        else if (derecha || izquierda)
        {
            rigid.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        }
    }

    /// <summary>
    /// Bloquea el desplazamiento de la caja
    /// </summary>
    public void parar()
    {
        rigid.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    /// <summary>
    /// Detectamos si colisiona con un objetivo
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("Caja trigger" + collision.gameObject.name);
        Objetivo objetivo = collision.gameObject.GetComponent<Objetivo>();

        if (objetivo != null)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = pressColor;
        }
    }

    /// <summary>
    /// Detectamos si sale de la colision con objetivo
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("Caja trigger" + collision.gameObject.name);

        Objetivo objetivo = collision.gameObject.GetComponent<Objetivo>();

        if (objetivo != null)
        {
            this.gameObject.GetComponent<SpriteRenderer>().color = originColor;
        }
    }
}
