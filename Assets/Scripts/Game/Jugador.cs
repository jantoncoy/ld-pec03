using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase que se encarga de gestionar al npc del jugador y sus acciones
/// </summary>
public class Jugador : MonoBehaviour
{
    /// <summary>
    /// Indicadores de movimiento
    /// </summary>
    private bool derecha, izquierda, arriba, abajo;

    /// <summary>
    /// Transform del jugador
    /// </summary>
    private Transform trans;

    /// <summary>
    /// Gestor de animaciones del jugador
    /// </summary>
    private Animator ani;

    /// <summary>
    /// Rigidbody del jugador
    /// </summary>
    private Rigidbody2D rigid;

    // Start is called before the first frame update
    void Start()
    {
        //Inicializamos movimiento
        derecha = false;
        izquierda = false;
        arriba = false;

        trans = this.gameObject.transform;
        ani = this.GetComponent<Animator>();
        rigid = this.GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// Procesamos el jugador
    /// </summary>
    private void FixedUpdate()
    {
        procesarInputs();
        procesarMovimiento();
    }

    /// <summary>
    /// Procesamos inputs
    /// </summary>
    private void procesarInputs()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (horizontalInput > 0)
        {
            this.izquierda = false;
            this.derecha = true;
        }
        else if (horizontalInput < 0)
        {
            this.izquierda = true;
            this.derecha = false;
        }
        else
        {
            this.derecha = false;
            this.izquierda = false;
        }

        if (vertical > 0)
        {
            this.abajo = false;
            this.arriba = true;
        }
        else if (vertical < 0)
        {
            this.abajo = true;
            this.arriba = false;
        }
        else
        {
            this.abajo = false;
            this.arriba = false;
        }
    }

    /// <summary>
    /// Procesamos movimientos
    /// </summary>
    private void procesarMovimiento()
    {
        
        if (arriba)
        {
            //nos movemos hacia arriba
            rigid.velocity = new Vector2(rigid.velocity.x,2);
        }
        else if (abajo)
        {
            //nos movemos hacia abajo
            rigid.velocity = new Vector2(rigid.velocity.x, -2);
        }

        if (derecha)
        {
            //nos movemos hacia la derecha
            rigid.velocity = new Vector2(2, rigid.velocity.y);
            trans.eulerAngles = new Vector3(trans.eulerAngles.x,0f,trans.eulerAngles.z);
        }
        else if (izquierda)
        {
            //nos movemos hacia la izquierda
            rigid.velocity = new Vector2(-2, rigid.velocity.y);
            trans.eulerAngles = new Vector3(trans.eulerAngles.x, 180f, trans.eulerAngles.z);    
        }

        if(arriba || abajo || derecha || izquierda)
        {
            if((arriba || abajo) && (derecha || izquierda))
            {
                rigid.constraints = RigidbodyConstraints2D.FreezeRotation;
            }else if(arriba || abajo)
            {
                rigid.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            }else if(izquierda || derecha)
            {
                rigid.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
            }
            GestorNivel.sumarPaso();
            ani.SetInteger("Estado", 1);
        }
        else
        {
            rigid.constraints = RigidbodyConstraints2D.FreezeAll;
            ani.SetInteger("Estado", 0);
        }
    }

    /// <summary>
    /// Detectamos las colisiones que permanecen
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionStay2D(Collision2D collision)
    {
        Caja caja = collision.gameObject.GetComponent<Caja>();

        if (caja != null && ani.GetInteger("Estado") == 1)
        {
            ani.SetInteger("Estado", 2);
            caja.moverse(arriba, abajo, izquierda, derecha);
            GestorNivel.sumarEmpuje();
        }else if(caja != null && ani.GetInteger("Estado") != 1)
        {
            caja.parar();
        }
    }

    /// <summary>
    /// Detectamos cuando deja de colisionar
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionExit2D(Collision2D collision)
    {
        Caja caja = collision.gameObject.GetComponent<Caja>();

        if (caja != null)
        {
            caja.parar();
        }
    }
}
