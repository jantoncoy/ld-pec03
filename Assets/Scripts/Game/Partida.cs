using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  Partida
{
    /// <summary>
    /// Nivel que cargara la escena nivel
    /// </summary>
    public static string nivel = "Niveles/Nivel_0";

    /// <summary>
    /// Nivel de entero
    /// </summary>
    public static int nivelInt = 0;

    /// <summary>
    /// Constantes de las rutas
    /// </summary>
    public static string nivelPath = "Nivel_",levelsPath = "Niveles/";

    /// <summary>
    /// Nivel maximo
    /// </summary>
    public static int nivelMax = 10;

}
