using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objetivo : MonoBehaviour
{
    /// <summary>
    /// Indica si esta cumplido
    /// </summary>
    public bool cumplido = false;

    // Start is called before the first frame update
    void Start()
    {
        //Inicializamos variables
        cumplido = false;

        //registramos el nivel
        GestorNivel.registrarObjetivo(this);   
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Caja caja = collision.gameObject.GetComponent<Caja>();

        if (caja != null)
        {
            this.cumplido = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Caja caja = collision.gameObject.GetComponent<Caja>();

        if (caja != null)
        {
            this.cumplido = false;
        }
    }
}
